ARG PYTHON_VERSION=3.9-alpine

FROM python:${PYTHON_VERSION} as builder
ENV PYTHONUNBUFFERED=1

WORKDIR /wheels

RUN apk add --update --no-cache \
    alpine-sdk \
    postgresql-dev

COPY Pipfile.lock .
COPY Pipfile .
RUN pip install pipenv &&  \
    pipenv lock -r > requirements.txt && \
    pip wheel -r requirements.txt --disable-pip-version-check

FROM python:${PYTHON_VERSION}
ENV PYTHONUNBUFFERED=1

RUN apk add --update --no-cache \
    libpq \
    postgresql-client

COPY --from=builder /wheels /wheels
RUN pip install \
        --no-cache-dir \
        --disable-pip-version-check \
        -r /wheels/requirements.txt \
        -f /wheels \
    && rm -rf /wheels

WORKDIR /app

COPY . /app

EXPOSE 8000

ENTRYPOINT ["python", "manage.py"]

CMD ["runserver", "0.0.0.0:8000"]